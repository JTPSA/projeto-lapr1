import junit.framework.JUnit4TestAdapter;
import org.junit.Test;

import static org.junit.Assert.*;

public class LAPR1_TurmaDCD_Grupo04_projetoTest {

    @Test
    public void vetor_proprio_inicial() {
        LAPR1_TurmaDCD_Grupo04_projeto teste3 = new LAPR1_TurmaDCD_Grupo04_projeto();

        double matComparacao[][]={{0.8, 0.3},{0.2,0.7}};
        Matrix a = new Basic2DMatrix(matComparacao);
        EigenDecompositor eigenD=new EigenDecompositor(a);
        Matrix [] mattD= eigenD.decompose();
        double matB [][]= mattD[1].toDenseMatrix().toArray();
        double matA [][]= mattD[0].toDenseMatrix().toArray();

        double[] teste = teste3.vetor_proprio_inicial(1,matB,matA);
        double[] valor_esperado = new double[2];
        valor_esperado[0]=matA[0][0];
        valor_esperado[1]=matA[1][0];
        assertArrayEquals(valor_esperado,teste,0);

    }
}